package reza.mars;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by payfazz87 on 18/04/18.
 */

public class ReadRoverNewConfiguration {


    static Scanner userInputReader = new Scanner(System.in);
    static String currentPosition = "";
    public static List<String> newPositions = new ArrayList<String>();

    /**
     * MarsRover current position
     */
    public static void readCurrentPosition() {
        System.out.println("Enter current position");
        currentPosition = userInputReader.nextLine();
        readMoveConfiguration();
    }

    /**
     * Read MarsRover new position
     */
    private static void readMoveConfiguration() {
        System.out.println("Enter first rover position : ");
        newPositions.add(userInputReader.nextLine());
        newPositions.add(userInputReader.nextLine());
        System.out.println("Enter second rover position : ");
        newPositions.add(userInputReader.nextLine());
        newPositions.add(userInputReader.nextLine());
    }
}
