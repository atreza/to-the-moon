package reza.mars;

/**
 * Created by payfazz87 on 18/04/18.
 */

public class Constant {

    public static final String NR = "NR";
    public static final String E = "E";
    public static final String NL = "NL";
    public static final String W = "W";
    public static final String WR = "WR";
    public static final String N = "N";
    public static final String WL = "WL";
    public static final String S = "S";
    public static final String SR = "SR";
    public static final String SL ="SL";
    public static final String ER = "ER";
    public static final String EL = "EL";
}
