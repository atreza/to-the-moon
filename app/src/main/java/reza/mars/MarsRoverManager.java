package reza.mars;

/**
 * Created by payfazz87 on 18/04/18.
 */

public class MarsRoverManager {
    public static int X = 0;
    public static int Y = 0;
    public static String compass = "";
    private static MarsRoverManager instance;


    public static synchronized MarsRoverManager getInstance() {
        if (instance == null){
            instance = new MarsRoverManager();
        }
        return instance;
    }


    public void sendToRover() {
        Util.getNewDirection(ReadRoverNewConfiguration.newPositions.get(0));
        startMovingProcess(X, Y, compass,
                Util.getNewDirectionPath(ReadRoverNewConfiguration.newPositions.get(1)));
        Util.getNewDirection(ReadRoverNewConfiguration.newPositions.get(2));
        startMovingProcess(X, Y, compass,
                Util.getNewDirectionPath(ReadRoverNewConfiguration.newPositions.get(3)));
    }

    
    private void startMovingProcess(int x, int y, String compass, char[] path) {
        for (int move = 0; move < path.length; move++) {
            switch (path[move]) {
                case 'M':
                    if (compass.equals("N") || compass.equals("S")) {
                        y = Util.getFormulaConstantsValue(y, compass);
                    } else {
                        x = Util.getFormulaConstantsValue(x, compass);
                    }
                    break;
                case 'R':
                    compass = Util.getNewCompass(compass + "R");
                    break;
                case 'L':
                    compass = Util.getNewCompass(compass + "L");
                    break;
            }
        }
        System.out.println(x + " " + y + " " + compass);
    }
}
