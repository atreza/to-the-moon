package reza.mars;

public class MarsActivity {

    public static void init() {
        ReadRoverNewConfiguration.readCurrentPosition();
        MarsRoverManager.getInstance().sendToRover();
    }
}
