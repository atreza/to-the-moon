package reza.mars;

import java.util.regex.Pattern;

import static reza.mars.Constant.E;
import static reza.mars.Constant.EL;
import static reza.mars.Constant.ER;
import static reza.mars.Constant.N;
import static reza.mars.Constant.NL;
import static reza.mars.Constant.NR;
import static reza.mars.Constant.S;
import static reza.mars.Constant.SL;
import static reza.mars.Constant.SR;
import static reza.mars.Constant.W;
import static reza.mars.Constant.WL;
import static reza.mars.Constant.WR;

/**
 * Created by payfazz87 on 18/04/18.
 */

public class Util {

    public static void getNewDirection(String directions){
        if(directions.contains(" ")) {
            String[] newDirection = Pattern.compile(" ").split(directions);
            if(newDirection.length==3) {
                MarsRoverManager.X = Integer.parseInt(newDirection[0].trim());
                MarsRoverManager.Y = Integer.parseInt(newDirection[1].trim());
                MarsRoverManager.compass = newDirection[2].trim();
            } else {
                System.out.println("New direction input error");
            }
        }
    }

    public static int getFormulaConstantsValue(int axisValue, String compass) {
        switch (compass) {
            case N:
                return ++axisValue;
            case E:
                return ++axisValue;
            case W:
                return --axisValue;
            case S:
                return --axisValue;
        }
        return 0;
    }

    public static char[] getNewDirectionPath(String jobToRover) {
        return jobToRover.toCharArray();
    }

    public static String getNewCompass(String compass) {
        switch (compass) {
            case NR:
                return E;
            case NL:
                return W;
            case WR:
                return N;
            case WL:
                return S;
            case SR:
                return W;
            case SL:
                return E;
            case ER:
                return S;
            case EL:
                return N;
        }
        return "";
    }
}
